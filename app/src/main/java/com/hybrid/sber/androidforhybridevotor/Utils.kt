package com.hybrid.sber.androidforhybridevotor

import io.reactivex.Observable
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL


fun createPostRequest(url:String,body:String) = Observable.create<String>{
    val urlConnection = URL(url).openConnection() as HttpURLConnection
    try {
//        urlConnection.connect()
        urlConnection.doOutput
        urlConnection.setChunkedStreamingMode(0)
        val out = urlConnection.outputStream
        out.write(body.toByteArray())
        urlConnection.connect()
        if (urlConnection.responseCode != HttpURLConnection.HTTP_OK)
            it.onError(RuntimeException(urlConnection.responseMessage))
        else {
            val str = urlConnection.inputStream.bufferedReader().readText()
            it.onNext(str)
        }
    } finally {
        urlConnection.disconnect()
    }
}


fun createRequest(url: String) = Observable.create<String> {
    val urlConnection = URL(url).openConnection() as HttpURLConnection
    try {
        urlConnection.connect()

        if (urlConnection.responseCode != HttpURLConnection.HTTP_OK)
            it.onError(RuntimeException(urlConnection.responseMessage))
        else {
            val str = urlConnection.inputStream.bufferedReader().readText()
            it.onNext(str)
        }
    } finally {
        urlConnection.disconnect()
    }
}