package com.hybrid.sber.androidforhybridevotor

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient


class SecondFragment : android.support.v4.app.Fragment() {

    lateinit var url: String
    lateinit var vBrowser: WebView
    lateinit var settings: WebSettings


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        url = arguments!!.getString("url")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater!!.inflate(R.layout.sec_fragment, container, false)

        vBrowser = view.findViewById<WebView>(R.id.frag2_browser)
        settings = vBrowser.settings
        settings.javaScriptEnabled

        vBrowser.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url);
                println("hello")
                return true
            }
        })

        return view

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        vBrowser.loadUrl(url)
    }

}