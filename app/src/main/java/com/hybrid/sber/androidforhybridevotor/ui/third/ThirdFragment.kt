package com.hybrid.sber.androidforhybridevotor.ui.third

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import com.hybrid.sber.androidforhybridevotor.*
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.ArrayList

class ThirdFragment : Fragment() {


     lateinit var vUrlRecView: RecyclerView
     lateinit var vBodyRecView: RecyclerView
     lateinit var urlEdit: EditText
     lateinit var bodyEdit: EditText
     var request: Disposable? = null
     val urlList = ArrayList<String>()
     val bodyList = ArrayList<String>()


    companion object {
        fun newInstance() = ThirdFragment()
    }

    private lateinit var viewModel: ThirdViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater!!.inflate(R.layout.third_fragment, container, false)

        var btn:Button = view.findViewById(R.id.buttonMakeRequest)
        var btnBody:Button = view.findViewById(R.id.buttonAddUrl)

        btn.setOnClickListener(View.OnClickListener {
            goToUrl(it)
        }
        )
        btnBody.setOnClickListener(View.OnClickListener {
            goToUrlBody(it)
        }
        )

        urlEdit = view.findViewById<EditText>(R.id.editTextUrl)
        bodyEdit = view.findViewById<EditText>(R.id.editBody)
        vUrlRecView = view.findViewById<RecyclerView>(R.id.act2_recView)
        vBodyRecView = view.findViewById<RecyclerView>(R.id.act3_recView)
        urlList.addAll(listOf("https://yandex.ru","https://online.sberbank.ru","http://consultant.ru/","http://ng.ru/"))
        bodyList.addAll(listOf("<order>" +
                                "<action>gr_li</action>" +
                                "<agent>gr_li</agent>" +
                                "<terminal>57474</terminal>" +
                                "<version_protocol>1.3.1</version_protocol>" +
                                "</order>","<order> <action>gr_li</action><agent>gr_li</agent><terminal>57474</terminal><version_protocol>1.3.1</version_protocol></order>",
            "<>","<>"))

        return view


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        var reAdaptUrl = ThirdActivity.RecUrlAdapter(urlList)
        var reAdaptBody = ThirdActivity.RecBodyAdapter(bodyList)
        vUrlRecView.adapter = reAdaptUrl
        vUrlRecView.layoutManager = LinearLayoutManager(activity)
        vBodyRecView.adapter = reAdaptBody
        vBodyRecView.layoutManager = LinearLayoutManager(activity)

    }


    fun putUrl(str:String){
        urlEdit.setText(str)
    }



    fun goToUrl(view: View){
        urlList.add(urlEdit.text.toString())
        var reAdaptUrl = ThirdActivity.RecUrlAdapter(urlList)
        vUrlRecView.adapter = reAdaptUrl
        vUrlRecView.layoutManager = LinearLayoutManager(activity)

    }
    fun goToUrlBody(view: View){
        bodyList.add(bodyEdit.text.toString())
        var reAdaptBody = ThirdActivity.RecBodyAdapter(bodyList)
        vBodyRecView.adapter = reAdaptBody
        vBodyRecView.layoutManager = LinearLayoutManager(activity)

    }

    class RecHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: String) {

            val vURL = itemView.findViewById<TextView>(R.id.url_str_item)

            vURL.text = item
//
            vURL.setOnClickListener(){
                Log.d("OnClcik","Onclick"+vURL.text.toString())
//                (vURL.context as ThirdActivity).goToURLActivity(vURL.text.toString())
            }

//            itemView.setOnClickListener {
//                (vURL.context as ThirdFragment).putUrl(vURL.text.toString())
//            }
        }
    }
}
