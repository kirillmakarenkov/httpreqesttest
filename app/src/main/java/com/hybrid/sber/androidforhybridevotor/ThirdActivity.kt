package com.hybrid.sber.androidforhybridevotor

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.hybrid.sber.androidforhybridevotor.ui.third.ThirdFragment
import kotlinx.android.synthetic.main.third_fragment.view.*
import java.util.ArrayList
import android.R.id.edit
import android.content.SharedPreferences.Editor



class ThirdActivity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_fragment)
        if (savedInstanceState == null) {
            val bundle = Bundle()
            bundle.putString("param", "value")
            val f = ThirdFragment()
            f.arguments = bundle
            supportFragmentManager.beginTransaction().replace(R.id.fragment_place, f).commitAllowingStateLoss()
        }
    }

    fun goToURLActivity(url:String){
        val bundle = Bundle()
        bundle.putString("url",url )
        val f = SecondFragment()
        f.arguments = bundle

        supportFragmentManager.beginTransaction().add(R.id.fragment_place, f).addToBackStack("main")
            .commitAllowingStateLoss()
    }

    class RecUrlAdapter(val items: ArrayList<String>) :
        RecyclerView.Adapter<com.hybrid.sber.androidforhybridevotor.ThirdActivity.RecHolder>() {

        @SuppressLint("ResourceType")
        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): com.hybrid.sber.androidforhybridevotor.ThirdActivity.RecHolder {
            val inflater = LayoutInflater.from(parent.context)

            val view = inflater.inflate(R.layout.url_item, parent, false)

            return RecHolder(view)
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: com.hybrid.sber.androidforhybridevotor.ThirdActivity.RecHolder, position: Int) {
            val item = items[position]!!

            holder.bind(item)
        }

        override fun getItemViewType(position: Int): Int {
            return super.getItemViewType(position)
        }

    }

    class RecHolder(view: View) : RecyclerView.ViewHolder(view),View.OnClickListener {
        override fun onClick(v: View?) {

        }

        fun bind(item: String) {

            val vURL = itemView.findViewById<TextView>(R.id.url_str_item)

            vURL.text = item
            itemView.setOnClickListener {
                    (vURL.context as ThirdActivity).goToURLActivity(vURL.text.toString())
            }
        }
    }

    class RecBodyAdapter(val items: ArrayList<String>) :
        RecyclerView.Adapter<com.hybrid.sber.androidforhybridevotor.ThirdActivity.RecBodyHolder>() {

        @SuppressLint("ResourceType")
        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): com.hybrid.sber.androidforhybridevotor.ThirdActivity.RecBodyHolder {
            val inflater = LayoutInflater.from(parent.context)

            val view = inflater.inflate(R.layout.url_item, parent, false)

            return RecBodyHolder(view)
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: com.hybrid.sber.androidforhybridevotor.ThirdActivity.RecBodyHolder, position: Int) {
            val item = items[position]!!

            holder.bind(item)
        }

        override fun getItemViewType(position: Int): Int {
            return super.getItemViewType(position)
        }

    }
    class  RecBodyHolder(view: View) : RecyclerView.ViewHolder(view),View.OnClickListener  {

        override fun onClick(v: View?) {

        }

        fun bind(item: String) {

            val vURL = itemView.findViewById<TextView>(R.id.url_str_item)


            vURL.text = item


            itemView.setOnClickListener {
//                (vURL.context as ThirdFragment).putUrl("dfdfdf")
                Log.d("OnClcik","Onclick"+vURL.text.toString())
            }
        }
    }

//    fun goBack(){
//        val f = ThirdFragment()
//        supportFragmentManager.beginTransaction().replace(R.id.fragment_place, f).commitAllowingStateLoss()
//    }

}
